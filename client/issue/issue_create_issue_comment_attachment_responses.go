// Code generated by go-swagger; DO NOT EDIT.

package issue

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"

	"gitea.com/jolheiser/go-gitea/models"
)

// IssueCreateIssueCommentAttachmentReader is a Reader for the IssueCreateIssueCommentAttachment structure.
type IssueCreateIssueCommentAttachmentReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *IssueCreateIssueCommentAttachmentReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 201:
		result := NewIssueCreateIssueCommentAttachmentCreated()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil
	case 400:
		result := NewIssueCreateIssueCommentAttachmentBadRequest()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	case 404:
		result := NewIssueCreateIssueCommentAttachmentNotFound()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	default:
		return nil, runtime.NewAPIError("response status code does not match any response statuses defined for this endpoint in the swagger spec", response, response.Code())
	}
}

// NewIssueCreateIssueCommentAttachmentCreated creates a IssueCreateIssueCommentAttachmentCreated with default headers values
func NewIssueCreateIssueCommentAttachmentCreated() *IssueCreateIssueCommentAttachmentCreated {
	return &IssueCreateIssueCommentAttachmentCreated{}
}

/*
IssueCreateIssueCommentAttachmentCreated describes a response with status code 201, with default header values.

Attachment
*/
type IssueCreateIssueCommentAttachmentCreated struct {
	Payload *models.Attachment
}

// IsSuccess returns true when this issue create issue comment attachment created response has a 2xx status code
func (o *IssueCreateIssueCommentAttachmentCreated) IsSuccess() bool {
	return true
}

// IsRedirect returns true when this issue create issue comment attachment created response has a 3xx status code
func (o *IssueCreateIssueCommentAttachmentCreated) IsRedirect() bool {
	return false
}

// IsClientError returns true when this issue create issue comment attachment created response has a 4xx status code
func (o *IssueCreateIssueCommentAttachmentCreated) IsClientError() bool {
	return false
}

// IsServerError returns true when this issue create issue comment attachment created response has a 5xx status code
func (o *IssueCreateIssueCommentAttachmentCreated) IsServerError() bool {
	return false
}

// IsCode returns true when this issue create issue comment attachment created response a status code equal to that given
func (o *IssueCreateIssueCommentAttachmentCreated) IsCode(code int) bool {
	return code == 201
}

// Code gets the status code for the issue create issue comment attachment created response
func (o *IssueCreateIssueCommentAttachmentCreated) Code() int {
	return 201
}

func (o *IssueCreateIssueCommentAttachmentCreated) Error() string {
	return fmt.Sprintf("[POST /repos/{owner}/{repo}/issues/comments/{id}/assets][%d] issueCreateIssueCommentAttachmentCreated  %+v", 201, o.Payload)
}

func (o *IssueCreateIssueCommentAttachmentCreated) String() string {
	return fmt.Sprintf("[POST /repos/{owner}/{repo}/issues/comments/{id}/assets][%d] issueCreateIssueCommentAttachmentCreated  %+v", 201, o.Payload)
}

func (o *IssueCreateIssueCommentAttachmentCreated) GetPayload() *models.Attachment {
	return o.Payload
}

func (o *IssueCreateIssueCommentAttachmentCreated) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Attachment)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewIssueCreateIssueCommentAttachmentBadRequest creates a IssueCreateIssueCommentAttachmentBadRequest with default headers values
func NewIssueCreateIssueCommentAttachmentBadRequest() *IssueCreateIssueCommentAttachmentBadRequest {
	return &IssueCreateIssueCommentAttachmentBadRequest{}
}

/*
IssueCreateIssueCommentAttachmentBadRequest describes a response with status code 400, with default header values.

APIError is error format response
*/
type IssueCreateIssueCommentAttachmentBadRequest struct {
	Message string
	URL     string
}

// IsSuccess returns true when this issue create issue comment attachment bad request response has a 2xx status code
func (o *IssueCreateIssueCommentAttachmentBadRequest) IsSuccess() bool {
	return false
}

// IsRedirect returns true when this issue create issue comment attachment bad request response has a 3xx status code
func (o *IssueCreateIssueCommentAttachmentBadRequest) IsRedirect() bool {
	return false
}

// IsClientError returns true when this issue create issue comment attachment bad request response has a 4xx status code
func (o *IssueCreateIssueCommentAttachmentBadRequest) IsClientError() bool {
	return true
}

// IsServerError returns true when this issue create issue comment attachment bad request response has a 5xx status code
func (o *IssueCreateIssueCommentAttachmentBadRequest) IsServerError() bool {
	return false
}

// IsCode returns true when this issue create issue comment attachment bad request response a status code equal to that given
func (o *IssueCreateIssueCommentAttachmentBadRequest) IsCode(code int) bool {
	return code == 400
}

// Code gets the status code for the issue create issue comment attachment bad request response
func (o *IssueCreateIssueCommentAttachmentBadRequest) Code() int {
	return 400
}

func (o *IssueCreateIssueCommentAttachmentBadRequest) Error() string {
	return fmt.Sprintf("[POST /repos/{owner}/{repo}/issues/comments/{id}/assets][%d] issueCreateIssueCommentAttachmentBadRequest ", 400)
}

func (o *IssueCreateIssueCommentAttachmentBadRequest) String() string {
	return fmt.Sprintf("[POST /repos/{owner}/{repo}/issues/comments/{id}/assets][%d] issueCreateIssueCommentAttachmentBadRequest ", 400)
}

func (o *IssueCreateIssueCommentAttachmentBadRequest) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// hydrates response header message
	hdrMessage := response.GetHeader("message")

	if hdrMessage != "" {
		o.Message = hdrMessage
	}

	// hydrates response header url
	hdrURL := response.GetHeader("url")

	if hdrURL != "" {
		o.URL = hdrURL
	}

	return nil
}

// NewIssueCreateIssueCommentAttachmentNotFound creates a IssueCreateIssueCommentAttachmentNotFound with default headers values
func NewIssueCreateIssueCommentAttachmentNotFound() *IssueCreateIssueCommentAttachmentNotFound {
	return &IssueCreateIssueCommentAttachmentNotFound{}
}

/*
IssueCreateIssueCommentAttachmentNotFound describes a response with status code 404, with default header values.

APIError is error format response
*/
type IssueCreateIssueCommentAttachmentNotFound struct {
	Message string
	URL     string
}

// IsSuccess returns true when this issue create issue comment attachment not found response has a 2xx status code
func (o *IssueCreateIssueCommentAttachmentNotFound) IsSuccess() bool {
	return false
}

// IsRedirect returns true when this issue create issue comment attachment not found response has a 3xx status code
func (o *IssueCreateIssueCommentAttachmentNotFound) IsRedirect() bool {
	return false
}

// IsClientError returns true when this issue create issue comment attachment not found response has a 4xx status code
func (o *IssueCreateIssueCommentAttachmentNotFound) IsClientError() bool {
	return true
}

// IsServerError returns true when this issue create issue comment attachment not found response has a 5xx status code
func (o *IssueCreateIssueCommentAttachmentNotFound) IsServerError() bool {
	return false
}

// IsCode returns true when this issue create issue comment attachment not found response a status code equal to that given
func (o *IssueCreateIssueCommentAttachmentNotFound) IsCode(code int) bool {
	return code == 404
}

// Code gets the status code for the issue create issue comment attachment not found response
func (o *IssueCreateIssueCommentAttachmentNotFound) Code() int {
	return 404
}

func (o *IssueCreateIssueCommentAttachmentNotFound) Error() string {
	return fmt.Sprintf("[POST /repos/{owner}/{repo}/issues/comments/{id}/assets][%d] issueCreateIssueCommentAttachmentNotFound ", 404)
}

func (o *IssueCreateIssueCommentAttachmentNotFound) String() string {
	return fmt.Sprintf("[POST /repos/{owner}/{repo}/issues/comments/{id}/assets][%d] issueCreateIssueCommentAttachmentNotFound ", 404)
}

func (o *IssueCreateIssueCommentAttachmentNotFound) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// hydrates response header message
	hdrMessage := response.GetHeader("message")

	if hdrMessage != "" {
		o.Message = hdrMessage
	}

	// hydrates response header url
	hdrURL := response.GetHeader("url")

	if hdrURL != "" {
		o.URL = hdrURL
	}

	return nil
}
