// Code generated by go-swagger; DO NOT EDIT.

package organization

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"

	"gitea.com/jolheiser/go-gitea/models"
)

// OrgCreateHookReader is a Reader for the OrgCreateHook structure.
type OrgCreateHookReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *OrgCreateHookReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 201:
		result := NewOrgCreateHookCreated()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil
	default:
		return nil, runtime.NewAPIError("response status code does not match any response statuses defined for this endpoint in the swagger spec", response, response.Code())
	}
}

// NewOrgCreateHookCreated creates a OrgCreateHookCreated with default headers values
func NewOrgCreateHookCreated() *OrgCreateHookCreated {
	return &OrgCreateHookCreated{}
}

/*
OrgCreateHookCreated describes a response with status code 201, with default header values.

Hook
*/
type OrgCreateHookCreated struct {
	Payload *models.Hook
}

// IsSuccess returns true when this org create hook created response has a 2xx status code
func (o *OrgCreateHookCreated) IsSuccess() bool {
	return true
}

// IsRedirect returns true when this org create hook created response has a 3xx status code
func (o *OrgCreateHookCreated) IsRedirect() bool {
	return false
}

// IsClientError returns true when this org create hook created response has a 4xx status code
func (o *OrgCreateHookCreated) IsClientError() bool {
	return false
}

// IsServerError returns true when this org create hook created response has a 5xx status code
func (o *OrgCreateHookCreated) IsServerError() bool {
	return false
}

// IsCode returns true when this org create hook created response a status code equal to that given
func (o *OrgCreateHookCreated) IsCode(code int) bool {
	return code == 201
}

// Code gets the status code for the org create hook created response
func (o *OrgCreateHookCreated) Code() int {
	return 201
}

func (o *OrgCreateHookCreated) Error() string {
	return fmt.Sprintf("[POST /orgs/{org}/hooks][%d] orgCreateHookCreated  %+v", 201, o.Payload)
}

func (o *OrgCreateHookCreated) String() string {
	return fmt.Sprintf("[POST /orgs/{org}/hooks][%d] orgCreateHookCreated  %+v", 201, o.Payload)
}

func (o *OrgCreateHookCreated) GetPayload() *models.Hook {
	return o.Payload
}

func (o *OrgCreateHookCreated) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Hook)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
