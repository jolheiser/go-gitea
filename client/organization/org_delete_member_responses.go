// Code generated by go-swagger; DO NOT EDIT.

package organization

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"

	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"
)

// OrgDeleteMemberReader is a Reader for the OrgDeleteMember structure.
type OrgDeleteMemberReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *OrgDeleteMemberReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 204:
		result := NewOrgDeleteMemberNoContent()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil
	default:
		return nil, runtime.NewAPIError("response status code does not match any response statuses defined for this endpoint in the swagger spec", response, response.Code())
	}
}

// NewOrgDeleteMemberNoContent creates a OrgDeleteMemberNoContent with default headers values
func NewOrgDeleteMemberNoContent() *OrgDeleteMemberNoContent {
	return &OrgDeleteMemberNoContent{}
}

/*
OrgDeleteMemberNoContent describes a response with status code 204, with default header values.

member removed
*/
type OrgDeleteMemberNoContent struct {
}

// IsSuccess returns true when this org delete member no content response has a 2xx status code
func (o *OrgDeleteMemberNoContent) IsSuccess() bool {
	return true
}

// IsRedirect returns true when this org delete member no content response has a 3xx status code
func (o *OrgDeleteMemberNoContent) IsRedirect() bool {
	return false
}

// IsClientError returns true when this org delete member no content response has a 4xx status code
func (o *OrgDeleteMemberNoContent) IsClientError() bool {
	return false
}

// IsServerError returns true when this org delete member no content response has a 5xx status code
func (o *OrgDeleteMemberNoContent) IsServerError() bool {
	return false
}

// IsCode returns true when this org delete member no content response a status code equal to that given
func (o *OrgDeleteMemberNoContent) IsCode(code int) bool {
	return code == 204
}

// Code gets the status code for the org delete member no content response
func (o *OrgDeleteMemberNoContent) Code() int {
	return 204
}

func (o *OrgDeleteMemberNoContent) Error() string {
	return fmt.Sprintf("[DELETE /orgs/{org}/members/{username}][%d] orgDeleteMemberNoContent ", 204)
}

func (o *OrgDeleteMemberNoContent) String() string {
	return fmt.Sprintf("[DELETE /orgs/{org}/members/{username}][%d] orgDeleteMemberNoContent ", 204)
}

func (o *OrgDeleteMemberNoContent) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
