// Code generated by go-swagger; DO NOT EDIT.

package organization

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"

	"gitea.com/jolheiser/go-gitea/models"
)

// OrgListHooksReader is a Reader for the OrgListHooks structure.
type OrgListHooksReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *OrgListHooksReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 200:
		result := NewOrgListHooksOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil
	default:
		return nil, runtime.NewAPIError("response status code does not match any response statuses defined for this endpoint in the swagger spec", response, response.Code())
	}
}

// NewOrgListHooksOK creates a OrgListHooksOK with default headers values
func NewOrgListHooksOK() *OrgListHooksOK {
	return &OrgListHooksOK{}
}

/*
OrgListHooksOK describes a response with status code 200, with default header values.

HookList
*/
type OrgListHooksOK struct {
	Payload []*models.Hook
}

// IsSuccess returns true when this org list hooks o k response has a 2xx status code
func (o *OrgListHooksOK) IsSuccess() bool {
	return true
}

// IsRedirect returns true when this org list hooks o k response has a 3xx status code
func (o *OrgListHooksOK) IsRedirect() bool {
	return false
}

// IsClientError returns true when this org list hooks o k response has a 4xx status code
func (o *OrgListHooksOK) IsClientError() bool {
	return false
}

// IsServerError returns true when this org list hooks o k response has a 5xx status code
func (o *OrgListHooksOK) IsServerError() bool {
	return false
}

// IsCode returns true when this org list hooks o k response a status code equal to that given
func (o *OrgListHooksOK) IsCode(code int) bool {
	return code == 200
}

// Code gets the status code for the org list hooks o k response
func (o *OrgListHooksOK) Code() int {
	return 200
}

func (o *OrgListHooksOK) Error() string {
	return fmt.Sprintf("[GET /orgs/{org}/hooks][%d] orgListHooksOK  %+v", 200, o.Payload)
}

func (o *OrgListHooksOK) String() string {
	return fmt.Sprintf("[GET /orgs/{org}/hooks][%d] orgListHooksOK  %+v", 200, o.Payload)
}

func (o *OrgListHooksOK) GetPayload() []*models.Hook {
	return o.Payload
}

func (o *OrgListHooksOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response payload
	if err := consumer.Consume(response.Body(), &o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
