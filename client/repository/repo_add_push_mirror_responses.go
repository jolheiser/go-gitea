// Code generated by go-swagger; DO NOT EDIT.

package repository

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"

	"gitea.com/jolheiser/go-gitea/models"
)

// RepoAddPushMirrorReader is a Reader for the RepoAddPushMirror structure.
type RepoAddPushMirrorReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *RepoAddPushMirrorReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 201:
		result := NewRepoAddPushMirrorCreated()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil
	case 400:
		result := NewRepoAddPushMirrorBadRequest()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	case 403:
		result := NewRepoAddPushMirrorForbidden()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	default:
		return nil, runtime.NewAPIError("response status code does not match any response statuses defined for this endpoint in the swagger spec", response, response.Code())
	}
}

// NewRepoAddPushMirrorCreated creates a RepoAddPushMirrorCreated with default headers values
func NewRepoAddPushMirrorCreated() *RepoAddPushMirrorCreated {
	return &RepoAddPushMirrorCreated{}
}

/*
RepoAddPushMirrorCreated describes a response with status code 201, with default header values.

PushMirror
*/
type RepoAddPushMirrorCreated struct {
	Payload *models.PushMirror
}

// IsSuccess returns true when this repo add push mirror created response has a 2xx status code
func (o *RepoAddPushMirrorCreated) IsSuccess() bool {
	return true
}

// IsRedirect returns true when this repo add push mirror created response has a 3xx status code
func (o *RepoAddPushMirrorCreated) IsRedirect() bool {
	return false
}

// IsClientError returns true when this repo add push mirror created response has a 4xx status code
func (o *RepoAddPushMirrorCreated) IsClientError() bool {
	return false
}

// IsServerError returns true when this repo add push mirror created response has a 5xx status code
func (o *RepoAddPushMirrorCreated) IsServerError() bool {
	return false
}

// IsCode returns true when this repo add push mirror created response a status code equal to that given
func (o *RepoAddPushMirrorCreated) IsCode(code int) bool {
	return code == 201
}

// Code gets the status code for the repo add push mirror created response
func (o *RepoAddPushMirrorCreated) Code() int {
	return 201
}

func (o *RepoAddPushMirrorCreated) Error() string {
	return fmt.Sprintf("[POST /repos/{owner}/{repo}/push_mirrors][%d] repoAddPushMirrorCreated  %+v", 201, o.Payload)
}

func (o *RepoAddPushMirrorCreated) String() string {
	return fmt.Sprintf("[POST /repos/{owner}/{repo}/push_mirrors][%d] repoAddPushMirrorCreated  %+v", 201, o.Payload)
}

func (o *RepoAddPushMirrorCreated) GetPayload() *models.PushMirror {
	return o.Payload
}

func (o *RepoAddPushMirrorCreated) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.PushMirror)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewRepoAddPushMirrorBadRequest creates a RepoAddPushMirrorBadRequest with default headers values
func NewRepoAddPushMirrorBadRequest() *RepoAddPushMirrorBadRequest {
	return &RepoAddPushMirrorBadRequest{}
}

/*
RepoAddPushMirrorBadRequest describes a response with status code 400, with default header values.

APIError is error format response
*/
type RepoAddPushMirrorBadRequest struct {
	Message string
	URL     string
}

// IsSuccess returns true when this repo add push mirror bad request response has a 2xx status code
func (o *RepoAddPushMirrorBadRequest) IsSuccess() bool {
	return false
}

// IsRedirect returns true when this repo add push mirror bad request response has a 3xx status code
func (o *RepoAddPushMirrorBadRequest) IsRedirect() bool {
	return false
}

// IsClientError returns true when this repo add push mirror bad request response has a 4xx status code
func (o *RepoAddPushMirrorBadRequest) IsClientError() bool {
	return true
}

// IsServerError returns true when this repo add push mirror bad request response has a 5xx status code
func (o *RepoAddPushMirrorBadRequest) IsServerError() bool {
	return false
}

// IsCode returns true when this repo add push mirror bad request response a status code equal to that given
func (o *RepoAddPushMirrorBadRequest) IsCode(code int) bool {
	return code == 400
}

// Code gets the status code for the repo add push mirror bad request response
func (o *RepoAddPushMirrorBadRequest) Code() int {
	return 400
}

func (o *RepoAddPushMirrorBadRequest) Error() string {
	return fmt.Sprintf("[POST /repos/{owner}/{repo}/push_mirrors][%d] repoAddPushMirrorBadRequest ", 400)
}

func (o *RepoAddPushMirrorBadRequest) String() string {
	return fmt.Sprintf("[POST /repos/{owner}/{repo}/push_mirrors][%d] repoAddPushMirrorBadRequest ", 400)
}

func (o *RepoAddPushMirrorBadRequest) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// hydrates response header message
	hdrMessage := response.GetHeader("message")

	if hdrMessage != "" {
		o.Message = hdrMessage
	}

	// hydrates response header url
	hdrURL := response.GetHeader("url")

	if hdrURL != "" {
		o.URL = hdrURL
	}

	return nil
}

// NewRepoAddPushMirrorForbidden creates a RepoAddPushMirrorForbidden with default headers values
func NewRepoAddPushMirrorForbidden() *RepoAddPushMirrorForbidden {
	return &RepoAddPushMirrorForbidden{}
}

/*
RepoAddPushMirrorForbidden describes a response with status code 403, with default header values.

APIForbiddenError is a forbidden error response
*/
type RepoAddPushMirrorForbidden struct {
	Message string
	URL     string
}

// IsSuccess returns true when this repo add push mirror forbidden response has a 2xx status code
func (o *RepoAddPushMirrorForbidden) IsSuccess() bool {
	return false
}

// IsRedirect returns true when this repo add push mirror forbidden response has a 3xx status code
func (o *RepoAddPushMirrorForbidden) IsRedirect() bool {
	return false
}

// IsClientError returns true when this repo add push mirror forbidden response has a 4xx status code
func (o *RepoAddPushMirrorForbidden) IsClientError() bool {
	return true
}

// IsServerError returns true when this repo add push mirror forbidden response has a 5xx status code
func (o *RepoAddPushMirrorForbidden) IsServerError() bool {
	return false
}

// IsCode returns true when this repo add push mirror forbidden response a status code equal to that given
func (o *RepoAddPushMirrorForbidden) IsCode(code int) bool {
	return code == 403
}

// Code gets the status code for the repo add push mirror forbidden response
func (o *RepoAddPushMirrorForbidden) Code() int {
	return 403
}

func (o *RepoAddPushMirrorForbidden) Error() string {
	return fmt.Sprintf("[POST /repos/{owner}/{repo}/push_mirrors][%d] repoAddPushMirrorForbidden ", 403)
}

func (o *RepoAddPushMirrorForbidden) String() string {
	return fmt.Sprintf("[POST /repos/{owner}/{repo}/push_mirrors][%d] repoAddPushMirrorForbidden ", 403)
}

func (o *RepoAddPushMirrorForbidden) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// hydrates response header message
	hdrMessage := response.GetHeader("message")

	if hdrMessage != "" {
		o.Message = hdrMessage
	}

	// hydrates response header url
	hdrURL := response.GetHeader("url")

	if hdrURL != "" {
		o.URL = hdrURL
	}

	return nil
}
