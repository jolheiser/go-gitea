// Code generated by go-swagger; DO NOT EDIT.

package repository

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"

	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"
)

// RepoDeleteReleaseAttachmentReader is a Reader for the RepoDeleteReleaseAttachment structure.
type RepoDeleteReleaseAttachmentReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *RepoDeleteReleaseAttachmentReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 204:
		result := NewRepoDeleteReleaseAttachmentNoContent()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil
	default:
		return nil, runtime.NewAPIError("response status code does not match any response statuses defined for this endpoint in the swagger spec", response, response.Code())
	}
}

// NewRepoDeleteReleaseAttachmentNoContent creates a RepoDeleteReleaseAttachmentNoContent with default headers values
func NewRepoDeleteReleaseAttachmentNoContent() *RepoDeleteReleaseAttachmentNoContent {
	return &RepoDeleteReleaseAttachmentNoContent{}
}

/*
RepoDeleteReleaseAttachmentNoContent describes a response with status code 204, with default header values.

APIEmpty is an empty response
*/
type RepoDeleteReleaseAttachmentNoContent struct {
}

// IsSuccess returns true when this repo delete release attachment no content response has a 2xx status code
func (o *RepoDeleteReleaseAttachmentNoContent) IsSuccess() bool {
	return true
}

// IsRedirect returns true when this repo delete release attachment no content response has a 3xx status code
func (o *RepoDeleteReleaseAttachmentNoContent) IsRedirect() bool {
	return false
}

// IsClientError returns true when this repo delete release attachment no content response has a 4xx status code
func (o *RepoDeleteReleaseAttachmentNoContent) IsClientError() bool {
	return false
}

// IsServerError returns true when this repo delete release attachment no content response has a 5xx status code
func (o *RepoDeleteReleaseAttachmentNoContent) IsServerError() bool {
	return false
}

// IsCode returns true when this repo delete release attachment no content response a status code equal to that given
func (o *RepoDeleteReleaseAttachmentNoContent) IsCode(code int) bool {
	return code == 204
}

// Code gets the status code for the repo delete release attachment no content response
func (o *RepoDeleteReleaseAttachmentNoContent) Code() int {
	return 204
}

func (o *RepoDeleteReleaseAttachmentNoContent) Error() string {
	return fmt.Sprintf("[DELETE /repos/{owner}/{repo}/releases/{id}/assets/{attachment_id}][%d] repoDeleteReleaseAttachmentNoContent ", 204)
}

func (o *RepoDeleteReleaseAttachmentNoContent) String() string {
	return fmt.Sprintf("[DELETE /repos/{owner}/{repo}/releases/{id}/assets/{attachment_id}][%d] repoDeleteReleaseAttachmentNoContent ", 204)
}

func (o *RepoDeleteReleaseAttachmentNoContent) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
