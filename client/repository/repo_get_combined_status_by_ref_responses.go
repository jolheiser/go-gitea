// Code generated by go-swagger; DO NOT EDIT.

package repository

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"

	"gitea.com/jolheiser/go-gitea/models"
)

// RepoGetCombinedStatusByRefReader is a Reader for the RepoGetCombinedStatusByRef structure.
type RepoGetCombinedStatusByRefReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *RepoGetCombinedStatusByRefReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 200:
		result := NewRepoGetCombinedStatusByRefOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil
	case 400:
		result := NewRepoGetCombinedStatusByRefBadRequest()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	default:
		return nil, runtime.NewAPIError("response status code does not match any response statuses defined for this endpoint in the swagger spec", response, response.Code())
	}
}

// NewRepoGetCombinedStatusByRefOK creates a RepoGetCombinedStatusByRefOK with default headers values
func NewRepoGetCombinedStatusByRefOK() *RepoGetCombinedStatusByRefOK {
	return &RepoGetCombinedStatusByRefOK{}
}

/*
RepoGetCombinedStatusByRefOK describes a response with status code 200, with default header values.

CombinedStatus
*/
type RepoGetCombinedStatusByRefOK struct {
	Payload *models.CombinedStatus
}

// IsSuccess returns true when this repo get combined status by ref o k response has a 2xx status code
func (o *RepoGetCombinedStatusByRefOK) IsSuccess() bool {
	return true
}

// IsRedirect returns true when this repo get combined status by ref o k response has a 3xx status code
func (o *RepoGetCombinedStatusByRefOK) IsRedirect() bool {
	return false
}

// IsClientError returns true when this repo get combined status by ref o k response has a 4xx status code
func (o *RepoGetCombinedStatusByRefOK) IsClientError() bool {
	return false
}

// IsServerError returns true when this repo get combined status by ref o k response has a 5xx status code
func (o *RepoGetCombinedStatusByRefOK) IsServerError() bool {
	return false
}

// IsCode returns true when this repo get combined status by ref o k response a status code equal to that given
func (o *RepoGetCombinedStatusByRefOK) IsCode(code int) bool {
	return code == 200
}

// Code gets the status code for the repo get combined status by ref o k response
func (o *RepoGetCombinedStatusByRefOK) Code() int {
	return 200
}

func (o *RepoGetCombinedStatusByRefOK) Error() string {
	return fmt.Sprintf("[GET /repos/{owner}/{repo}/commits/{ref}/status][%d] repoGetCombinedStatusByRefOK  %+v", 200, o.Payload)
}

func (o *RepoGetCombinedStatusByRefOK) String() string {
	return fmt.Sprintf("[GET /repos/{owner}/{repo}/commits/{ref}/status][%d] repoGetCombinedStatusByRefOK  %+v", 200, o.Payload)
}

func (o *RepoGetCombinedStatusByRefOK) GetPayload() *models.CombinedStatus {
	return o.Payload
}

func (o *RepoGetCombinedStatusByRefOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.CombinedStatus)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewRepoGetCombinedStatusByRefBadRequest creates a RepoGetCombinedStatusByRefBadRequest with default headers values
func NewRepoGetCombinedStatusByRefBadRequest() *RepoGetCombinedStatusByRefBadRequest {
	return &RepoGetCombinedStatusByRefBadRequest{}
}

/*
RepoGetCombinedStatusByRefBadRequest describes a response with status code 400, with default header values.

APIError is error format response
*/
type RepoGetCombinedStatusByRefBadRequest struct {
	Message string
	URL     string
}

// IsSuccess returns true when this repo get combined status by ref bad request response has a 2xx status code
func (o *RepoGetCombinedStatusByRefBadRequest) IsSuccess() bool {
	return false
}

// IsRedirect returns true when this repo get combined status by ref bad request response has a 3xx status code
func (o *RepoGetCombinedStatusByRefBadRequest) IsRedirect() bool {
	return false
}

// IsClientError returns true when this repo get combined status by ref bad request response has a 4xx status code
func (o *RepoGetCombinedStatusByRefBadRequest) IsClientError() bool {
	return true
}

// IsServerError returns true when this repo get combined status by ref bad request response has a 5xx status code
func (o *RepoGetCombinedStatusByRefBadRequest) IsServerError() bool {
	return false
}

// IsCode returns true when this repo get combined status by ref bad request response a status code equal to that given
func (o *RepoGetCombinedStatusByRefBadRequest) IsCode(code int) bool {
	return code == 400
}

// Code gets the status code for the repo get combined status by ref bad request response
func (o *RepoGetCombinedStatusByRefBadRequest) Code() int {
	return 400
}

func (o *RepoGetCombinedStatusByRefBadRequest) Error() string {
	return fmt.Sprintf("[GET /repos/{owner}/{repo}/commits/{ref}/status][%d] repoGetCombinedStatusByRefBadRequest ", 400)
}

func (o *RepoGetCombinedStatusByRefBadRequest) String() string {
	return fmt.Sprintf("[GET /repos/{owner}/{repo}/commits/{ref}/status][%d] repoGetCombinedStatusByRefBadRequest ", 400)
}

func (o *RepoGetCombinedStatusByRefBadRequest) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// hydrates response header message
	hdrMessage := response.GetHeader("message")

	if hdrMessage != "" {
		o.Message = hdrMessage
	}

	// hydrates response header url
	hdrURL := response.GetHeader("url")

	if hdrURL != "" {
		o.URL = hdrURL
	}

	return nil
}
