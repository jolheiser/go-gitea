// Code generated by go-swagger; DO NOT EDIT.

package repository

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"

	"gitea.com/jolheiser/go-gitea/models"
)

// RepoListStatusesByRefReader is a Reader for the RepoListStatusesByRef structure.
type RepoListStatusesByRefReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *RepoListStatusesByRefReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 200:
		result := NewRepoListStatusesByRefOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil
	case 400:
		result := NewRepoListStatusesByRefBadRequest()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	default:
		return nil, runtime.NewAPIError("response status code does not match any response statuses defined for this endpoint in the swagger spec", response, response.Code())
	}
}

// NewRepoListStatusesByRefOK creates a RepoListStatusesByRefOK with default headers values
func NewRepoListStatusesByRefOK() *RepoListStatusesByRefOK {
	return &RepoListStatusesByRefOK{}
}

/*
RepoListStatusesByRefOK describes a response with status code 200, with default header values.

CommitStatusList
*/
type RepoListStatusesByRefOK struct {
	Payload []*models.CommitStatus
}

// IsSuccess returns true when this repo list statuses by ref o k response has a 2xx status code
func (o *RepoListStatusesByRefOK) IsSuccess() bool {
	return true
}

// IsRedirect returns true when this repo list statuses by ref o k response has a 3xx status code
func (o *RepoListStatusesByRefOK) IsRedirect() bool {
	return false
}

// IsClientError returns true when this repo list statuses by ref o k response has a 4xx status code
func (o *RepoListStatusesByRefOK) IsClientError() bool {
	return false
}

// IsServerError returns true when this repo list statuses by ref o k response has a 5xx status code
func (o *RepoListStatusesByRefOK) IsServerError() bool {
	return false
}

// IsCode returns true when this repo list statuses by ref o k response a status code equal to that given
func (o *RepoListStatusesByRefOK) IsCode(code int) bool {
	return code == 200
}

// Code gets the status code for the repo list statuses by ref o k response
func (o *RepoListStatusesByRefOK) Code() int {
	return 200
}

func (o *RepoListStatusesByRefOK) Error() string {
	return fmt.Sprintf("[GET /repos/{owner}/{repo}/commits/{ref}/statuses][%d] repoListStatusesByRefOK  %+v", 200, o.Payload)
}

func (o *RepoListStatusesByRefOK) String() string {
	return fmt.Sprintf("[GET /repos/{owner}/{repo}/commits/{ref}/statuses][%d] repoListStatusesByRefOK  %+v", 200, o.Payload)
}

func (o *RepoListStatusesByRefOK) GetPayload() []*models.CommitStatus {
	return o.Payload
}

func (o *RepoListStatusesByRefOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response payload
	if err := consumer.Consume(response.Body(), &o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewRepoListStatusesByRefBadRequest creates a RepoListStatusesByRefBadRequest with default headers values
func NewRepoListStatusesByRefBadRequest() *RepoListStatusesByRefBadRequest {
	return &RepoListStatusesByRefBadRequest{}
}

/*
RepoListStatusesByRefBadRequest describes a response with status code 400, with default header values.

APIError is error format response
*/
type RepoListStatusesByRefBadRequest struct {
	Message string
	URL     string
}

// IsSuccess returns true when this repo list statuses by ref bad request response has a 2xx status code
func (o *RepoListStatusesByRefBadRequest) IsSuccess() bool {
	return false
}

// IsRedirect returns true when this repo list statuses by ref bad request response has a 3xx status code
func (o *RepoListStatusesByRefBadRequest) IsRedirect() bool {
	return false
}

// IsClientError returns true when this repo list statuses by ref bad request response has a 4xx status code
func (o *RepoListStatusesByRefBadRequest) IsClientError() bool {
	return true
}

// IsServerError returns true when this repo list statuses by ref bad request response has a 5xx status code
func (o *RepoListStatusesByRefBadRequest) IsServerError() bool {
	return false
}

// IsCode returns true when this repo list statuses by ref bad request response a status code equal to that given
func (o *RepoListStatusesByRefBadRequest) IsCode(code int) bool {
	return code == 400
}

// Code gets the status code for the repo list statuses by ref bad request response
func (o *RepoListStatusesByRefBadRequest) Code() int {
	return 400
}

func (o *RepoListStatusesByRefBadRequest) Error() string {
	return fmt.Sprintf("[GET /repos/{owner}/{repo}/commits/{ref}/statuses][%d] repoListStatusesByRefBadRequest ", 400)
}

func (o *RepoListStatusesByRefBadRequest) String() string {
	return fmt.Sprintf("[GET /repos/{owner}/{repo}/commits/{ref}/statuses][%d] repoListStatusesByRefBadRequest ", 400)
}

func (o *RepoListStatusesByRefBadRequest) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// hydrates response header message
	hdrMessage := response.GetHeader("message")

	if hdrMessage != "" {
		o.Message = hdrMessage
	}

	// hydrates response header url
	hdrURL := response.GetHeader("url")

	if hdrURL != "" {
		o.URL = hdrURL
	}

	return nil
}
