// Code generated by go-swagger; DO NOT EDIT.

package user

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"

	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"
)

// UserCurrentDeleteStarReader is a Reader for the UserCurrentDeleteStar structure.
type UserCurrentDeleteStarReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *UserCurrentDeleteStarReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 204:
		result := NewUserCurrentDeleteStarNoContent()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil
	default:
		return nil, runtime.NewAPIError("response status code does not match any response statuses defined for this endpoint in the swagger spec", response, response.Code())
	}
}

// NewUserCurrentDeleteStarNoContent creates a UserCurrentDeleteStarNoContent with default headers values
func NewUserCurrentDeleteStarNoContent() *UserCurrentDeleteStarNoContent {
	return &UserCurrentDeleteStarNoContent{}
}

/*
UserCurrentDeleteStarNoContent describes a response with status code 204, with default header values.

APIEmpty is an empty response
*/
type UserCurrentDeleteStarNoContent struct {
}

// IsSuccess returns true when this user current delete star no content response has a 2xx status code
func (o *UserCurrentDeleteStarNoContent) IsSuccess() bool {
	return true
}

// IsRedirect returns true when this user current delete star no content response has a 3xx status code
func (o *UserCurrentDeleteStarNoContent) IsRedirect() bool {
	return false
}

// IsClientError returns true when this user current delete star no content response has a 4xx status code
func (o *UserCurrentDeleteStarNoContent) IsClientError() bool {
	return false
}

// IsServerError returns true when this user current delete star no content response has a 5xx status code
func (o *UserCurrentDeleteStarNoContent) IsServerError() bool {
	return false
}

// IsCode returns true when this user current delete star no content response a status code equal to that given
func (o *UserCurrentDeleteStarNoContent) IsCode(code int) bool {
	return code == 204
}

// Code gets the status code for the user current delete star no content response
func (o *UserCurrentDeleteStarNoContent) Code() int {
	return 204
}

func (o *UserCurrentDeleteStarNoContent) Error() string {
	return fmt.Sprintf("[DELETE /user/starred/{owner}/{repo}][%d] userCurrentDeleteStarNoContent ", 204)
}

func (o *UserCurrentDeleteStarNoContent) String() string {
	return fmt.Sprintf("[DELETE /user/starred/{owner}/{repo}][%d] userCurrentDeleteStarNoContent ", 204)
}

func (o *UserCurrentDeleteStarNoContent) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
