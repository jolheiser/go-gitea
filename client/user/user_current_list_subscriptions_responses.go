// Code generated by go-swagger; DO NOT EDIT.

package user

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"

	"gitea.com/jolheiser/go-gitea/models"
)

// UserCurrentListSubscriptionsReader is a Reader for the UserCurrentListSubscriptions structure.
type UserCurrentListSubscriptionsReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *UserCurrentListSubscriptionsReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 200:
		result := NewUserCurrentListSubscriptionsOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil
	default:
		return nil, runtime.NewAPIError("response status code does not match any response statuses defined for this endpoint in the swagger spec", response, response.Code())
	}
}

// NewUserCurrentListSubscriptionsOK creates a UserCurrentListSubscriptionsOK with default headers values
func NewUserCurrentListSubscriptionsOK() *UserCurrentListSubscriptionsOK {
	return &UserCurrentListSubscriptionsOK{}
}

/*
UserCurrentListSubscriptionsOK describes a response with status code 200, with default header values.

RepositoryList
*/
type UserCurrentListSubscriptionsOK struct {
	Payload []*models.Repository
}

// IsSuccess returns true when this user current list subscriptions o k response has a 2xx status code
func (o *UserCurrentListSubscriptionsOK) IsSuccess() bool {
	return true
}

// IsRedirect returns true when this user current list subscriptions o k response has a 3xx status code
func (o *UserCurrentListSubscriptionsOK) IsRedirect() bool {
	return false
}

// IsClientError returns true when this user current list subscriptions o k response has a 4xx status code
func (o *UserCurrentListSubscriptionsOK) IsClientError() bool {
	return false
}

// IsServerError returns true when this user current list subscriptions o k response has a 5xx status code
func (o *UserCurrentListSubscriptionsOK) IsServerError() bool {
	return false
}

// IsCode returns true when this user current list subscriptions o k response a status code equal to that given
func (o *UserCurrentListSubscriptionsOK) IsCode(code int) bool {
	return code == 200
}

// Code gets the status code for the user current list subscriptions o k response
func (o *UserCurrentListSubscriptionsOK) Code() int {
	return 200
}

func (o *UserCurrentListSubscriptionsOK) Error() string {
	return fmt.Sprintf("[GET /user/subscriptions][%d] userCurrentListSubscriptionsOK  %+v", 200, o.Payload)
}

func (o *UserCurrentListSubscriptionsOK) String() string {
	return fmt.Sprintf("[GET /user/subscriptions][%d] userCurrentListSubscriptionsOK  %+v", 200, o.Payload)
}

func (o *UserCurrentListSubscriptionsOK) GetPayload() []*models.Repository {
	return o.Payload
}

func (o *UserCurrentListSubscriptionsOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response payload
	if err := consumer.Consume(response.Body(), &o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
