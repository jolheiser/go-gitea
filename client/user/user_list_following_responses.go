// Code generated by go-swagger; DO NOT EDIT.

package user

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"

	"gitea.com/jolheiser/go-gitea/models"
)

// UserListFollowingReader is a Reader for the UserListFollowing structure.
type UserListFollowingReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *UserListFollowingReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 200:
		result := NewUserListFollowingOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil
	default:
		return nil, runtime.NewAPIError("response status code does not match any response statuses defined for this endpoint in the swagger spec", response, response.Code())
	}
}

// NewUserListFollowingOK creates a UserListFollowingOK with default headers values
func NewUserListFollowingOK() *UserListFollowingOK {
	return &UserListFollowingOK{}
}

/*
UserListFollowingOK describes a response with status code 200, with default header values.

UserList
*/
type UserListFollowingOK struct {
	Payload []*models.User
}

// IsSuccess returns true when this user list following o k response has a 2xx status code
func (o *UserListFollowingOK) IsSuccess() bool {
	return true
}

// IsRedirect returns true when this user list following o k response has a 3xx status code
func (o *UserListFollowingOK) IsRedirect() bool {
	return false
}

// IsClientError returns true when this user list following o k response has a 4xx status code
func (o *UserListFollowingOK) IsClientError() bool {
	return false
}

// IsServerError returns true when this user list following o k response has a 5xx status code
func (o *UserListFollowingOK) IsServerError() bool {
	return false
}

// IsCode returns true when this user list following o k response a status code equal to that given
func (o *UserListFollowingOK) IsCode(code int) bool {
	return code == 200
}

// Code gets the status code for the user list following o k response
func (o *UserListFollowingOK) Code() int {
	return 200
}

func (o *UserListFollowingOK) Error() string {
	return fmt.Sprintf("[GET /users/{username}/following][%d] userListFollowingOK  %+v", 200, o.Payload)
}

func (o *UserListFollowingOK) String() string {
	return fmt.Sprintf("[GET /users/{username}/following][%d] userListFollowingOK  %+v", 200, o.Payload)
}

func (o *UserListFollowingOK) GetPayload() []*models.User {
	return o.Payload
}

func (o *UserListFollowingOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response payload
	if err := consumer.Consume(response.Body(), &o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
