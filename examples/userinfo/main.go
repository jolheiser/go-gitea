package main

import (
	"fmt"
	"os"

	"gitea.com/jolheiser/go-gitea/client"
	httptransport "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/strfmt"
)

func main() {
	auth := httptransport.APIKeyAuth("Authorization", "header", "token "+os.Getenv("GITEA_TOKEN"))
	cl := client.NewHTTPClientWithConfig(strfmt.Default, &client.TransportConfig{
		Host:     "try.gitea.io",
		BasePath: client.DefaultBasePath,
		Schemes:  client.DefaultSchemes,
	})
	u, err := cl.User.UserGetCurrent(nil, auth)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s <%s>\n", u.Payload.UserName, u.Payload.Email)
}
