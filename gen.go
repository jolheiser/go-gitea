//go:build generate

package main

import (
	"io"
	"net/http"
	"os"
)

//go:generate go run gen.go
//go:generate go run github.com/go-swagger/go-swagger/cmd/swagger@latest generate client -f swagger.json

func main() {
	res, err := http.Get("https://try.gitea.io/swagger.v1.json")
	if err != nil {
		panic(err)
	}
	defer res.Body.Close()

	fi, err := os.Create("swagger.json")
	if err != nil {
		panic(err)
	}
	defer fi.Close()

	if _, err := io.Copy(fi, res.Body); err != nil {
		panic(err)
	}
}
